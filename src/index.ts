import 'dotenv/config'
import express from 'express'
import path from 'path'
import { fileURLToPath } from 'url';
import rootRoute from './routes/root.js'
import userRoutes from './routes/userRoutes.js';
import noteRoutes from './routes/noteRoutes.js';
import { logger } from './middleware/logger.js';
import { errHandler } from './middleware/errHandler.js';
import cookieParser from 'cookie-parser'
import cors from 'cors'
import { corsOptions } from './config/corsOptions.js';
import { connectDB } from './config/dbConn.js';
import mongoose from 'mongoose';
import { logEvents } from './middleware/logger.js';
import bodyParser from 'body-parser';

const app = express()
const PORT = process.env.PORT || 3500
const __filename = fileURLToPath(import.meta.url);
const __dirname = path.dirname(__filename);


console.log(process.env.NODE_ENV);
connectDB()

app.use(logger)
app.use(cors(corsOptions))
app.use(express.json())
app.use(cookieParser())
//public is relative to index.ts
app.use('/', express.static(path.join(__dirname, '..', 'public')))
app.use('/', rootRoute)
app.use('/users', userRoutes)
app.use('/notes', noteRoutes)

app.all('*', (req, res) => {
    res.status(404)
    if (req.accepts('html')) {
        res.sendFile(path.join(__dirname, 'views', '404.html'))
    } else if (req.accepts('json')) {
        res.json({ message: '404 Not Found' })
    } else {
        res.type('txt').send('404 Not Found')
    }
})

app.use(errHandler)

mongoose.connection.once('open', () => {
    console.log('Connected to MongoDB');
    app.listen(PORT, () => console.log(`Server running on port ${PORT}`))
})

mongoose.connection.on('error', err => {
    logEvents(`${err.no}\t${err.code}\t${err.syscall}\t${err.hostname}`, 'mongoErrLog.log')
})