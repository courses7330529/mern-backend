import express from 'express'
const router = express.Router()

import { getAllUsers, createUser, updateUser, deleteUser } from '../controllers/usersController.js'

router.route('/')
   .get(getAllUsers)
   .post(createUser)
   .patch(updateUser)
   .delete(deleteUser)

export default router