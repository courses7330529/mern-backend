import User from "../models/User.js";
import Note from "../models/Note.js";

import asyncHandler from 'express-async-handler'
import bcrypt from 'bcrypt'


export const getAllUsers = asyncHandler(async (_req, res: any) => {
    const users = await User.find().select('-password').lean()

    //.length? не мога да го достъпя
    if (!users.length) {
        return res.status(200).json({ message: 'No user found' })
    }

    res.json(users)
})

export const createUser = asyncHandler(async (req, res: any) => {
    const { username, password, roles } = req.body

    if (!username || !password || !Array.isArray(roles) || !roles.length) {
        return res.status(400).json({ message: 'All fields are required!' })
    }

    const duplicate = await User.findOne({ username }).lean().exec()
    if (duplicate) {
        return res.status(409).json({ message: 'duplicate username' })
    }

    const hashedPwd = await bcrypt.hash(password, 10)

    const userObject = { username, "password": hashedPwd, roles }

    const user = await User.create(userObject)
    if (user) {
        res.status(201).json({ message: `New user ${username} created` })
    } else {
        res.status(400).json({ message: 'Invalid user data received' })
    }

})

export const updateUser = asyncHandler(async (req, res: any) => {
    const { id, username, roles, active, password } = req.body
    if (!id || !username || !Array.isArray(roles) || !roles.length || typeof active !== 'boolean') {
        return res.status(400).json({ message: 'All fields are required' })
    }

    const user = await User.findById(id).exec()
    if (!user) {
        return res.status(400).json({ message: 'User not found' })
    }

    const duplicate = await User.findOne({ username }).lean().exec()
    if (duplicate && duplicate?._id.toString() !== id) {
        return res.status(400).json({ message: 'Duplicate username' })
    }

    user.username = username
    user.roles = roles
    user.active = active

    if (password) {
        user.password = await bcrypt.hash(password, 10)
    }

    const updatedUser = await user.save()

    res.json({ message: `${updatedUser.username} updated` })

})

export const deleteUser = asyncHandler(async (req, res: any) => {

    const { id } = req.body
    if (!id) {
        return res.status(400).json({ message: 'User ID Required' })
    }

    const notes = await Note.findOne({ user: id }).lean().exec()

    console.log(notes);
    //.length??? това е обект, къф length
    if (notes !== null) {
        return res.status(400).json({ message: 'User has assigned notes' })
    }

    const user = await User.findById(id).exec()
    if (!user) {
        return res.status(400).json({ message: 'User not found' })
    }

    const result = await user.deleteOne()
    const reply = `Username ${result.username} with ID ${result._id} deleted`
    res.json(reply)
})