import User from "../models/User.js";
import Note from "../models/Note.js";

import asyncHandler from 'express-async-handler'
import bcrypt from 'bcrypt'


export const getAllNotes = asyncHandler(async (_req, res: any) => {
    const notes = await Note.find().lean()

    //.length? не мога да го достъпя
    if (!notes.length) {
        return res.status(200).json({ message: 'No notes found' })
    }

    res.json(notes)
})

export const createNote = asyncHandler(async (req, res: any) => {
    const { user, title, text } = req.body
    console.log(user, title, text);

    if (!user || !title || !text) {
        return res.status(400).json({ message: 'All fields are required!' })
    }

    const duplicate = await Note.findOne({ title }).lean().exec()

    if (duplicate) {
        return res.status(409).json({ message: 'duplicate note title' })
    }

    const note = await Note.create({ user, title, text })

    if (note) {
        res.status(201).json({ message: `New note created` })
    } else {
        res.status(400).json({ message: 'Invalid note data received' })
    }

})

export const updateNote = asyncHandler(async (req, res: any) => {
    const { id, user, title, text, completed } = req.body
    if (!id || !user || !user || !text || typeof completed !== 'boolean') {
        return res.status(400).json({ message: 'All fields are required' })
    }

    const note = await Note.findById(id).exec()
    if (!note) {
        return res.status(400).json({ message: 'Note not found' })
    }

    const duplicate = await User.findOne({ title }).lean().exec()

    if (duplicate && duplicate?._id.toString() !== id) {
        return res.status(400).json({ message: 'Duplicate note title' })
    }

    note.user = user
    note.title = title
    note.text = text
    note.completed = completed

    const updatedNote = await note.save()

    res.json({ message: `${updatedNote.title} updated` })

})

export const deleteNote = asyncHandler(async (req, res: any) => {
    const { id } = req.body
    if (!id) {
        return res.status(400).json({ message: 'Note ID Required' })
    }

    const note = await Note.findById(id).exec()
    if (!note) {
        return res.status(400).json({ message: `No note with ${id} available` })
    }

    const result = await note.deleteOne()

    const reply = `Note with ID ${result._id} deleted`
    res.json(reply)
})