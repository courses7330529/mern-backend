import { whitelist } from "./whitelist.js";

export const corsOptions = {
    origin: (origin, cb) => {
        if (whitelist.indexOf(origin) !== -1 || !origin) {
            cb(null, true)
        } else {
            cb(new Error('Not allowed by CORS'))
        }
    },
    credentials: true,
    optionsSuccessStatus: 200
}