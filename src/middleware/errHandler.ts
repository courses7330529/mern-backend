import { logEvents } from "./logger.js";

export const errHandler = (err, req, res, next) => {
    logEvents(`${req.method}\t${req.url}\t${err.message}\t${req.headers.origin}`, 'errLog.log')

    // console.log(err.stack);

    const status = res.statusCode ? res.statusCode : 500

    res.status(status)

    res.json({ message: err.message })
}